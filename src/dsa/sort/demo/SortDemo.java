package dsa.sort.demo;

import dsa.sort.bubble.BubbleSort;
import dsa.sort.quick.QuickSort;
import dsa.sort.util.Util;

public class SortDemo {

	static Util util = new Util();

	public static void main(String[] args) {

		int numArray[] = { 64, 34, 25, 12, 22, 11, 90 };
		BubbleSort.bubbleSort(numArray);
		System.out.println("Bubble sorted array");
		util.printArray(numArray);

		int arr[] = { 10, 7, 8, 9, 1, 5 };
		int n = arr.length;

		QuickSort.sort(arr, 0, n - 1);
		System.out.println("Quick sorted array");
		util.printArray(arr);

	}
}
